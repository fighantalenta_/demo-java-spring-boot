package com.example.demo.controller;

import com.example.demo.model.Car;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class HomeController {

//    @GetMapping
//    public String home(@RequestHeader(name = "x-token-id") String token ) {
//        return token;
//    }
//
//    @GetMapping("/car/{id}")
//    public String getById(@PathVariable("id") String id) {
//        return id;
//    }

    @PostMapping("/car")
    public Car addCar(@RequestBody Car car) {
        return  car;
    }
}
